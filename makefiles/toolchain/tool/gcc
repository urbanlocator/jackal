## ***************************************************************************
## TITLE
##      GNU Compiler Collection
## PROJECT
##      Jackal/Common/Tools
## ***************************************************************************
##
## FILE
##      $Id:$
## HISTORY
##      $Log:$
## ***************************************************************************

## ---------------------------------------------------------------------------
## --------------------------------- BASE ------------------------------------
## ---------------------------------------------------------------------------

GCC             := $(GCC_PREFIX)gcc$(GCC_POSTFIX)
GPP             := $(GCC_PREFIX)g++$(GCC_POSTFIX)
GLIB            := $(GCC_PREFIX)ar
GLD             := $(GCC_PREFIX)ld
GOBJ            := $(GCC_PREFIX)objcopy
GSIZE            := $(GCC_PREFIX)size
ifneq (,$(GCC_PKG_CONFIG_PATH))
PKGCFG  		:= env PKG_CONFIG_PATH=$(GCC_PKG_CONFIG_PATH) $(GCC_PREFIX)pkg-config
else
PKGCFG  		:= $(GCC_PREFIX)pkg-config
endif

ifneq (,$(GCC_VERSION))
GCC             := $(GCC)-$(GCC_VERSION)
GPP             := $(GPP)-$(GCC_VERSION)
GLIB            := $(GLIB)-$(GCC_VERSION)
GOBJ            := $(GOBJ)-$(GCC_VERSION)
GLD             := $(GLD)-$(GCC_VERSION)
GSIZE           := $(GSIZE)-$(GCC_VERSION)
endif

GCC_CC          ?= $(GCC)$(HOST_EXT_BIN)
GCC_CPP         ?= $(GPP)$(HOST_EXT_BIN)
GCC_LIB         ?= $(GLIB)$(HOST_EXT_BIN)
GCC_OBJCOPY     ?= $(GOBJ)$(HOST_EXT_BIN)
GCC_LD          ?= $(GLD)$(HOST_EXT_BIN)
GCC_SIZE        ?= $(GSIZE)$(HOST_EXT_BIN)

GCC_OPTIMIZATION ?= -O2

TOOL_DESC       := GNU Compiler Collection
TOOL_EXT_LIB    := .lib

# -Wcast-align -no-canonical-prefixes
GCC_OPTS_BASE				+= -Wall $(foreach inc,$(BUILD_INCLUDE),-I"$(inc)") $(OPT_DEBUGTRACEWAY) -Wfatal-errors 
GCC_OPTS_LINK_BASE			+= $(if $(GCC_ROOT),-B $(GCC_ROOT)/bin) $(foreach lib,$(BUILD_LIBS_1ST), -l$(lib))
GCC_OPTS_LINK_POST          += $(foreach lib,$(SOURCE_BINLIBRARIES) $(BUILD_LIBS_2ND), -l$(lib)) 
GCC_LINK_OPT				:= -Wl,

FUNC_LIST_DYNLIB		= $(call FUNC_CMD,LIST  ,$(1),$(HOST_ECHO) '$(foreach file,$(3),$(file)\n)' > $(2))

GCC_CPP_BASE				+= -Wno-psabi

ifneq (,$(OPT_C99))
GCC_CC_BASE				+= -std=c99
endif

ifneq (,$(OPT_GNU99))
GCC_CC_BASE				+= -std=gnu99
endif


ifneq (,$(OPT_GNU11))
GCC_CPP_BASE				+= -std=gnu++11
endif

ifneq (,$(OPT_CPP11))
GCC_CPP_BASE				+= -std=c++11
endif

GCC_CPP_BASE				+= -fpermissive


ifneq (,$(OPT_PACKSTRUCT))
GCC_OPTS_BASE				+= -fpack-struct -Wno-pragmas
endif

ifneq (,$(OPT_STATICLINK))
GCC_OPTS_BASE				+= -static -static-libgcc
GCC_OPTS_LINK_BASE			+= -static -static-libgcc
endif

ifeq (,$(GCC_NOGCSECTIONS))
GCC_GCSECS_LD		:= $(GCC_LINK_OPT)--gc-sections
GCC_GCSECS_XLINKER	:= -Xlinker --gc-sections
endif

ifeq (macho,$(OPT_BINFORMAT))
GCC_BINFORMAT_BASE :=
GCC_BINFORMAT_LINK_BASE :=
else # Defalult: elf
GCC_BINFORMAT_BASE :=
GCC_BINFORMAT_LINK_BASE := $(if $(OPT_NOSTRIP),,$(GCC_LINK_OPT)-s)  $(GCC_GCSECS_LD)
GCC_OPTS_LINK_BASE			+= $(GCC_GCSECS_XLINKER)
endif

GCC_OPTS_BASE				+= $(if $(GCC_NOSECTIONS),,-fdata-sections -ffunction-sections)
GCC_OPTS_LINK_BASE 			+= $(if $(GCC_NOSECTIONS),,-fdata-sections -ffunction-sections)


ifeq (,$(OPT_DEBUG))
GCC_OPTS_LINK_BASE			+= $(GCC_BINFORMAT_LINK_BASE)
GCC_OPTS_BASE				+= $(GCC_OPTIMIZATION)
GCC_OPTS_LINK_BASE 			+= $(GCC_OPTIMIZATION)  
else
ifneq (,$(findstring PROFILER,$(OPT_DEBUG)))
GCC_OPTS_BASE				+= -pg
GCC_OPTS_LINK_BASE			+= -pg
else
#GCC_OPTS_BASE				+= -g3 -DDEBUG
GCC_OPTS_BASE				+= -ggdb3 -DDEBUG
endif
ifneq (,$(findstring EFENCE,$(OPT_DEBUG)))
GCC_OPTS_LINK_POST			+= -lefence
endif
endif

ifneq (,$(USE_BOOTLD))
GCC_OPTS_BASE				+= -DUSE_BOOTLD
endif

ifneq (,$(findstring mysqlclient,$(SOURCE_BINLIBRARIES)))
GCC_OPTS_LINK_BASE			+= -L$(if $(GCC_ROOT),$(GCC_ROOT),/usr)/lib/mysql
endif

ifneq (,$(GCC_OPTS_LINK_DIRS))
GCC_OPTS_LINK_BASE			+= $(foreach dr,$(GCC_OPTS_LINK_DIRS), -L$(dr))
endif

ifneq (,$(OPT_DEPLOY))
GCC_OPTS_BASE				+= -DDEPLOY
endif

ifneq (,$(GCC_MAKE_MAP))
FUNC_MAP =-Wl,-Map,$(1).map
endif

ifeq (Cortex-M0,$(BUILD_CPU_MODEL))
GCC_CPP_BASE += -fno-rtti -fno-exceptions -fno-use-cxa-atexit -fno-threadsafe-statics $(BEAN_GCC_CPP_BASE)
GCC_OPTS_BASE += -mcpu=cortex-m0 -mthumb -DCORE_M0
GCC_OPTS_LINK_BASE += -mcpu=cortex-m0 -mthumb -T $(GCC_LINK_SCRIPT_DIR)src/linkscript$(if $(OPT_LDCHIP),_$(OPT_LDCHIP))$(if $(OPT_DEBUG),_DEBUG,$(if $(USE_BOOTLD),_bootld)).ld
endif

ifeq (Cortex-M3,$(BUILD_CPU_MODEL))
GCC_CPP_BASE += -fno-rtti -fno-exceptions -fno-use-cxa-atexit -fno-threadsafe-statics $(BEAN_GCC_CPP_BASE)
GCC_OPTS_BASE += -mcpu=cortex-m3 -mthumb -DCORE_M3
GCC_OPTS_LINK_BASE += -mcpu=cortex-m3 -mthumb -T $(GCC_LINK_SCRIPT_DIR)src/linkscript$(if $(OPT_LDCHIP),_$(OPT_LDCHIP))$(if $(OPT_DEBUG),_DEBUG,$(if $(USE_BOOTLD),_bootld)).ld
endif

ifeq (Cortex-M4,$(BUILD_CPU_MODEL))
ifeq (SPD16,$(GCC_FPU_ABI))
GCC_OPTS_BASE  += -mfpu=fpv4-sp-d16 -mfloat-abi=hard
GCC_OPTS_LINK_BASE += -mfpu=fpv4-sp-d16 -mfloat-abi=hard
endif
GCC_CPP_BASE += -fno-rtti -fno-exceptions -fno-use-cxa-atexit -fno-threadsafe-statics $(BEAN_GCC_CPP_BASE)
GCC_OPTS_BASE += -mcpu=cortex-m4 -mthumb -DCORE_M4
GCC_OPTS_LINK_BASE += -mcpu=cortex-m4 -mthumb -T $(GCC_LINK_SCRIPT_DIR)src/linkscript$(if $(OPT_LDCHIP),_$(OPT_LDCHIP))$(if $(OPT_DEBUG),_DEBUG,$(if $(USE_BOOTLD),_bootld)).ld
endif

ifeq (Cortex-M7,$(BUILD_CPU_MODEL))
# -mfloat-abi=hard -mfpu=fpv5-sp-d16 -std=gnu11 -D__weak=__attribute__((weak)) -D__packed=__attribute__((__packed__)) -DUSE_HAL_DRIVER -DSTM32F746xx -I../Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1 -Os -ffunction-sections -fdata-sections -g -fstack-usage -Wall -specs=nano.specs -o Drivers/CMSIS/DSP/Source/FilteringFunctions/arm_conv_fast_opt_q15.o ../Drivers/CMSIS/DSP/Source/FilteringFunctions/arm_conv_fast_opt_q15.c 
ifeq (SPD16,$(GCC_FPU_ABI))
GCC_OPTS_BASE  += -mfpu=fpv5-sp-d16 -mfloat-abi=hard
GCC_OPTS_LINK_BASE += -mfpu=fpv5-sp-d16 -mfloat-abi=hard
endif
GCC_CPP_BASE += -fno-rtti -fno-exceptions -fno-use-cxa-atexit -fno-threadsafe-statics $(BEAN_GCC_CPP_BASE)
GCC_OPTS_BASE += -mcpu=cortex-m7 -mthumb -DCORE_M7
GCC_OPTS_LINK_BASE += -mcpu=cortex-m7 -mthumb -T $(GCC_LINK_SCRIPT_DIR)src/linkscript$(if $(OPT_LDCHIP),_$(OPT_LDCHIP))$(if $(OPT_DEBUG),_DEBUG,$(if $(USE_BOOTLD),_bootld)).ld
endif


ifneq (,$(SOURCE_TOOLKITS))
GCC_OPTS_BASE += $(foreach kit,$(SOURCE_TOOLKITS), $(shell $(PKGCFG) --cflags $(kit)))
GCC_OPTS_BASE += -fvisibility=hidden
ifeq (,$(GCC_GTK_NO_RDYNAMIC))
GCC_OPTS_LINK_BASE += -rdynamic 
endif
GCC_OPTS_LINK_POST += $(foreach kit,$(SOURCE_TOOLKITS), $(shell $(PKGCFG) --libs $(kit)))  
endif



## ---------------------------------------------------------------------------
## ----------------------------- ASM COMPILER --------------------------------
## ---------------------------------------------------------------------------

TOOL_ASM_EXT_SRC		:= .s
TOOL_ASM_EXT_OBJ		:= .o

FUNC_ASM_COMPILE			= $(call FUNC_CMD,ASM   ,$(2),no asm configured)


## ---------------------------------------------------------------------------
## ------------------------------ C COMPILER ---------------------------------
## ---------------------------------------------------------------------------

TOOL_CC_EXT_SRC			:= .c
TOOL_CC_EXT_OBJ			:= .o

TOOL_S_EXT_SRC			:= .S
TOOL_S_EXT_OBJ			:= .o

GCC_CC_OPTS					= $(GCC_OPTS_BASE) $(GCC_CC_BASE) $(BUILD_OPTS) 

FUNC_CC_BUILD			= $(call FUNC_CMD,BUILD ,$(1),$(GCC_CC) $(GCC_CC_OPTS) -o$(O_SPACE)"$(1)" "$(2)")
FUNC_CC_COMPILE			= $(call FUNC_CMD,CC    ,$(2),$(GCC_CC) -c $(GCC_CC_OPTS) $(call FUNC_DEBUG_OPTS,$(2)) -o "$(1)" "$(2)")



## ---------------------------------------------------------------------------
## ----------------------------- C++ COMPILER --------------------------------
## ---------------------------------------------------------------------------

#FUNC_C_GET_INCLUDES		:= @$(GCC_CC) -print-file-name=include
FUNC_C_GET_INCLUDES		:= @echo $(GCC_INCLUDES)

O_SPACE_BE				:= 
O_SPACE                                     := $(O_SPACE_BE) $(O_SPACE_BE)
TOOL_CPP_EXT_SRC		:= .cpp

TOOL_CPP_EXT_OBJ		:= $(TOOL_CC_EXT_OBJ) 
#  -fvtable-gc  -fno-exceptions -fno-rtti
GCC_CPP_OPTS				= $(GCC_OPTS_BASE) $(GCC_CPP_BASE) $(BUILD_OPTS)



FUNC_CPP_COMPILE		= $(call FUNC_CMD,CPP   ,$(2),$(GCC_CPP) -c $(GCC_CPP_OPTS) $(call FUNC_DEBUG_OPTS,$(2)) -o "$(1)" "$(2)")


## ---------------------------------------------------------------------------
## -------------------------------- LINKER -----------------------------------
## ---------------------------------------------------------------------------

ifneq (,$(GCC_USE_LD))
GCC_OPTS_LINK_BASE 		= $(GCC_LD_PARAMS)
GCC_LINK				= $(GCC_LD)
GCC_OPTS_LINK_POST_RAW	= $(GCC_LD_PARAMS_POST)
else
GCC_LINK				= $(if $(OPT_CPP),$(GCC_CPP),$(GCC_CC))
endif

FUNC_LINK_BIN			= $(call FUNC_CMD,BUILD ,$(1),$(GCC_LINK) $(GCC_OPTS_LINK_BASE) $(call FUNC_MAP,$(1)) -o "$(1)" $(2) $(3) $(if $(4), /RC:$(4)) $(GCC_OPTS_LINK_POST_RAW) $(foreach opt,$(GCC_OPTS_LINK_POST),$(GCC_LINK_OPT)$(opt)))
FUNC_LINK_BIN_LIST		= $(call FUNC_CMD,BUILD ,$(1),$(GCC_LINK) $(GCC_OPTS_LINK_BASE) $(call FUNC_MAP,$(1)) -o "$(1)" @$(2) $(3) $(if $(4), /RC:$(4)) $(GCC_OPTS_LINK_POST_RAW) $(foreach opt,$(GCC_OPTS_LINK_POST),$(GCC_LINK_OPT)$(opt)))
FUNC_LINK_HEX			= $(call FUNC_CMD,HEX   ,$(1),$(GCC_OBJCOPY) -j .text -j .data -O ihex $(3) $(1); $(GCC_OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O ihex $(3) $(2))

# 1 - Binary, 2 - import lib, 3 - objects list, 4 - libs
FUNC_LINK_DYNLIB		= $(call FUNC_CMD,BUILD ,$(1),$(GCC_CC) $(GCC_OPTS_LINK_BASE) -shared $(GCC_LINK_OPT)-o$(O_SPACE)$(1) $(shell cat $(3)) $(4) $(foreach opt,$(GCC_OPTS_LINK_POST),$(GCC_LINK_OPT)$(opt)))

## ---------------------------------------------------------------------------
## ------------------------------ LIBRARIAN ----------------------------------
## ---------------------------------------------------------------------------

# gnu binutils variant
#FUNC_LIB				= $(call FUNC_CMD,LIB   ,$(1),$(GCC_LIB) -M < "$(2)")
#FUNC_LIST_LIB          	= $(call FUNC_CMD,LIST  ,$(1),$(HOST_ECHO) 'create $(1)\n $(foreach file,$(3),addmod $(file)\n)save\n' > $(2))

# simplified for BSD/Darwin
FUNC_LIST_LINK          = $(call FUNC_CMD,LIST  ,$(1),$(HOST_ECHO) '$(foreach file,$(3),$(call FUNC_HOSTPATH,$(file)))' > $(2))
FUNC_LIST_LIB           = $(call FUNC_CMD,LIST  ,$(1),$(HOST_ECHO) 'rm -f $(1)\n $(foreach file,$(3),$(GCC_LIB) -cq $(1) $(file)\n)' > $(2))
FUNC_LIB				= $(call FUNC_CMD,LIB   ,$(1),chmod 755 "$(2)" ; "$(2)")
