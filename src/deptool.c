// ***************************************************************************
// TITLE
//      Main module
// PROJECT
//      Arsenal Library/Building System/Project Tool
// ***************************************************************************
// FILE
//      $Id: prjtool.c,v 1.7 2003/08/06 12:25:58 A.Kozhukhar Exp $
// AUTHOR
//      $Author: A.Kozhukhar $
// ***************************************************************************


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include <limits.h>

#include <sys/stat.h>
#include <unistd.h>



// ---------------------------------------------------------------------------
// -------------------------- LOCAL DEFINITIONS ------------------------------
// ---------------------------------------------------------------------------

#define FALSE                      0
#define TRUE                       (!FALSE)

#define DEF_JKL_ROOT                "JKL"

#define DEF_LINE_LEN                256

#define DEF_SWT_verbose             0x00000001
#define DEF_SWT_double_colons       0x00000004

#define DEF_NEED_HDR_LISTS          0x00000001

#define DEF_CYGWIN_PREFIX           "/cygdrive/"
#define DEF_CYGWIN_PREFIX_SZ        10

#define FLIST_FILES                 2000

// ---------------------------------------------------------------------------
// ----------------------------- LOCAL TYPES ---------------------------------
// ---------------------------------------------------------------------------

// ***************************************************************************
// ENUMERATION
//      ACTION
// PURPOSR
//      Action ID
// ***************************************************************************
typedef enum
{
    ACTION_UNKNOWN,
    ACTION_MAKE_DEPS,
    ACTION_INC,
    ACTION_DATE,
    ACTION_CONV_PATH
} ACTION;

typedef struct __tag_FLIST
{
    int         count;

    char *      seen        [ FLIST_FILES ];
} FLIST, * PFLIST;


// ***************************************************************************
// STRUCTURE
//      MKH_CONFIG
// PURPOSR
//      Make Helper configuration structure
// ***************************************************************************
typedef struct __tag_MKH_CONFIG
{
    char*       psz_hdrs_skip;
    char*       psz_hdrs_standard;
    char*       psz_hdrs_ext_target;
    ACTION      v_action;
    long        d_switches;
    char        sz_name                 [ DEF_LINE_LEN ];
    char        sz_config_dir           [ PATH_MAX  ];
} MKH_CONFIG, * PMKH_CONFIG;


// ---------------------------------------------------------------------------
// ----------------------------- STATIC DATA ---------------------------------
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ------------------------------ FUNCTIONS ----------------------------------
// ---------------------------------------------------------------------------

PFLIST flist_create()
{
    PFLIST      result = NULL;

    if ((result = malloc(sizeof(FLIST))) != NULL)
    {
        result->count = 0;
    }

    return result;
}
PFLIST flist_destroy(PFLIST list)
{
    int cnt;

    if (list)
    {
        for (cnt = 0; cnt < list->count; cnt++)
        {
            free(list->seen[cnt]);
        }
        free(list);
    }

    return NULL;
}
int flist_checkin(PFLIST   list,
               char *   fname)
{
    int     result      = 1;
    int     cnt;
    int     size;

    for (cnt = 0; result && (cnt < list->count); cnt++)
    {
        if (!strcmp(list->seen[cnt], fname))
        {
            result = 0;
        }
    }

    if (result)
    {
        if (list->count < FLIST_FILES)
        {
            size = strlen(fname) + 1;

            if ((list->seen[list->count] = malloc(size)) != NULL)
            {
                memcpy(list->seen[list->count], fname, size);

                list->count++;
            }
        }
        else
            printf("LIST COUNT OVERFLOW! \n");
    }

    return result;
}
// ***************************************************************************
// FUNCTION
//      mkh_path_create_to_file
// PURPOSE
//      Create path to file
// PARAMETERS
//      char* psz_path -- Pointer to file path and name
// RESULT
//      int   -- TRUE if all is ok or FALSE if error has occured
// ***************************************************************************
int  mkh_path_create_to_file(char* psz_path)
{
    int         b_result        = FALSE;
    char*       psz_tmp;
    char*       psz_tmp2;
    long        d_len;
    char        sz_cwd          [ PATH_MAX ];
    char        sz_create_path  [ PATH_MAX ];


    if (psz_path)
    {
        if (getcwd(sz_cwd, PATH_MAX))
        {
            if ((psz_tmp = strchr(psz_path, ':')) != NULL)
                psz_tmp += (2 * sizeof(char));
            else
                psz_tmp = psz_path + sizeof(char);

            for ( b_result = TRUE;
                  b_result && ( ((psz_tmp2 = strchr(psz_tmp, '/'))  != NULL) ||
                                ((psz_tmp2 = strchr(psz_tmp, '\\')) != NULL) ); )
            {
                d_len = psz_tmp2 - psz_path;
                memcpy(sz_create_path, psz_path, d_len);
                *(sz_create_path + d_len) = 0;

                b_result = !chdir(sz_create_path) ||
#if (TARGET_FAMILY == __AX_unix__)
                           (!mkdir(sz_create_path) // , -1)
#else
                           (!mkdir(sz_create_path)
#endif
                           		|| (errno == EEXIST));

                psz_tmp = ++psz_tmp2;
            }

           chdir(sz_cwd);
        }
    }

    return b_result;
}
// ***************************************************************************
// FUNCTION
//      mkh_list_find
// PURPOSE
//      Find string in list
// PARAMETERS
//      char* psz_list     -- Pointer to List
//      char* psz_string   -- Pointer to String to find
// RESULT
//      int   -- TRUE if all is ok or FALSE if error has occured
// ***************************************************************************
int  mkh_list_find(char* psz_list, char* psz_string)
{
    int     b_result        = FALSE;
    char*   psz_on_list     = psz_list;

    for ( ;
          *psz_on_list && !b_result;
          psz_on_list += (strlen(psz_on_list) + sizeof(char)))
        b_result = !strcmp(psz_on_list, psz_string);

    return b_result;
}
// ***************************************************************************
// FUNCTION
//      mkh_list_load
// PURPOSE
//      Load list
// PARAMETERS
//      char* psz_filename --
// RESULT
//      char*   --
// ***************************************************************************
char* mkh_list_load(PMKH_CONFIG pst_conf, char* psz_filename)
{
    char*       psz_list    = NULL;
    FILE *      pst_file;
    char*       psz_tmp;
    long        d_size;
    char        sz_string   [ PATH_MAX ] ;


    sprintf(sz_string, "%s/%s", pst_conf->sz_config_dir, psz_filename);

    if ((pst_file = fopen(sz_string, "rt")) != NULL)
    {
        if ( !fseek(pst_file, 0, SEEK_END)      &&
             ((d_size = ftell(pst_file)) != 0)  &&
             !fseek(pst_file, 0, SEEK_SET)      )
        {
            if ((psz_tmp = psz_list = malloc(d_size + sizeof(char))) != NULL)
            {
                while (fgets(psz_tmp, DEF_LINE_LEN, pst_file))
                {
                    psz_tmp += strlen(psz_tmp);
                    while (*psz_tmp < 33 && (psz_tmp > psz_list))
                        psz_tmp--;
                    *(++psz_tmp) = 0;
                    psz_tmp++;
                }

                *psz_tmp = 0;
            }
        }

        fclose(pst_file);
    }

    return psz_list;
}
int mkh_action_conv_path2(char * psz_path)
{
    int     i_result        = 0;
    char    sz_path         [ PATH_MAX ];

    switch (*psz_path)
    {
        case '/':
        case '\\':
            if (!memcmp(DEF_CYGWIN_PREFIX, psz_path, DEF_CYGWIN_PREFIX_SZ))
            {
                strcpy(sz_path, psz_path + DEF_CYGWIN_PREFIX_SZ - 1);

                *(sz_path)      = *(sz_path + 1);
                *(sz_path + 1)  = ':';

                strcpy(psz_path, sz_path);
            }
            break;

        default:
            break;
    }

    return i_result;
}

// ***************************************************************************
// FUNCTION
//      mkh_file_find_and_open
// PURPOSE
//      Find and open header file
// PARAMETERS
//      char*  psz_name -- Pointer to file name
// RESULT
//      FILE *  -- Pointer to the open file if found, or NULL
// ***************************************************************************
FILE * mkh_file_find_and_open(PMKH_CONFIG pst_conf, char* psz_name,
                              int * isStandard)
{
    FILE *  pst_file        = NULL;
    char*   psz_vpath;
    char*   psz_on_vpath;
    char*   psz_on_file;
    long    d_size;
    char*   psz_on_list     = pst_conf->psz_hdrs_standard;
    char    sz_file         [ DEF_LINE_LEN + 1 ];

    *isStandard = 0;

    if ((psz_on_vpath = psz_vpath = getenv("VPATH")) != NULL)
    {
// printf("VPATH = %s\n", psz_vpath);

        // Searching in VPATH

        while (!pst_file && *psz_on_vpath)
        {
            while (*psz_on_vpath && (*psz_on_vpath <= ' '))
                psz_on_vpath++;

            for (  psz_on_file = sz_file, d_size = DEF_LINE_LEN;
                   d_size && *psz_on_vpath && (*psz_on_vpath > ' ');
                   d_size--)
                *(psz_on_file++) = *(psz_on_vpath++) ;

            *psz_on_file = 0;
            d_size       = (psz_on_file - sz_file);

            sprintf(sz_file + d_size, "/%s", psz_name);

		mkh_action_conv_path2(sz_file);

            if ((pst_file = fopen(sz_file, "rt")) != NULL)
            {
                if (pst_conf->d_switches & DEF_SWT_verbose)
                printf(" (%s)", sz_file);
            }
        }

        // Searching in Standard headers


        for ( ;
                *psz_on_list && !pst_file;
                psz_on_list += (strlen(psz_on_list) + sizeof(char)))
        {
            sprintf(sz_file, "%s/%s", psz_on_list, psz_name);
            
            if ((pst_file = fopen(sz_file, "rt")) != NULL)
            {
                *isStandard = 1;
                if (pst_conf->d_switches & DEF_SWT_verbose)
                    printf(" (%s)", sz_file);
            }
        }

    }

    return pst_file;
}
// ***************************************************************************
// FUNCTION
//      mkh_deps_extract
// PURPOSE
//      Extract dependencies from file
// PARAMETERS
//      PMKH_CONFIG     pst_conf        -- Pointer to configuration structure
//      FILE          * pst_dep_file    -- Pointer to the dependencies file
//      FILE          * pst_src_file    -- Pointer to source file
//      char*           psz_target_path -- Pointer to targte file path
// RESULT
//      int
//          TRUE:  All is ok
//          FALSE: An error has occured
// ***************************************************************************
int  mkh_deps_extract(PMKH_CONFIG   pst_conf,
                      FILE *        pst_dep_file,
                      FILE *        pst_src_file,
                      char*         psz_target_path,
                      long          d_level,
                      PFLIST        flist)
{
    int     b_result        = TRUE;
    int     isStandard;
    char    c_quota;
    char*   psz_tmp;
    long    d_cnt;
    FILE *  pst_new_file;
    char    sz_string       [ DEF_LINE_LEN + 1 ];
    char    sz_file         [          512 + 1 ];
    char    sz_ext          [           32 + 1 ];


    while (b_result && fgets(sz_string, DEF_LINE_LEN, pst_src_file))
    {
        if (*sz_string == '#')
        {
            for (psz_tmp = sz_string + 1; *psz_tmp <= ' ';)
                psz_tmp++;

            if (!memcmp(psz_tmp, "include", 7))
            {
                if (sscanf(sz_string, "%*[^<\"]%c %512[^>\"]", &c_quota, sz_file) != 3)
                {
                    if (pst_conf->d_switches & DEF_SWT_verbose)
                    {
                        printf("%s: ", pst_conf->sz_name);

                        for (d_cnt = d_level; d_cnt; d_cnt--)
                            printf("  ");

                        printf("file %s", sz_file);
                    }

                    if (!mkh_list_find(pst_conf->psz_hdrs_skip, sz_file))
                    {
                        if (flist_checkin(flist, sz_file))
                        {
                            if ( !sscanf(sz_file, "%*[^.].%s", sz_ext)                  ||
                                 !mkh_list_find(pst_conf->psz_hdrs_ext_target, sz_ext)  )
                            {
                                if ( ((c_quota == '\"') && ((pst_new_file = fopen(sz_file, "rt")) != NULL)) ||
                                     ((pst_new_file = mkh_file_find_and_open(pst_conf, sz_file, &isStandard))  != NULL)  )
                                {
                                    if (!isStandard)
                                    {
                                        if (pst_conf->d_switches & DEF_SWT_verbose)
                                            printf(" - entering in...\n");

                                        b_result = mkh_deps_extract(pst_conf, pst_dep_file,
                                                                    pst_new_file, psz_target_path, d_level + 1, flist);

                                        fprintf(pst_dep_file, "\t\\\n \t\t%s", sz_file);
                                    }

                                    fclose(pst_new_file);
                                }
                                else
                                {
                                    if (pst_conf->d_switches & DEF_SWT_verbose)
                                        printf("%s: cannot find file: %s\n", pst_conf->sz_name, sz_file);
                                }
                            }
                            else
                            {
                                if (pst_conf->d_switches & DEF_SWT_verbose)
                                    printf(" - posed as target header\n");

                                fprintf(pst_dep_file, "\t\\\n \t\t%s/%s", psz_target_path, sz_file);
                            }
                        }
                        else
                        {
                            if (pst_conf->d_switches & DEF_SWT_verbose)
                                printf(" - seen header\n");
                        }
                    }
                    else
                    {
                        if (pst_conf->d_switches & DEF_SWT_verbose)
                            printf(" - skipped\n");

                        fprintf(pst_dep_file, "\t\\\n \t\t%s", sz_file);
                    }
                }
                else
                {
                    if (pst_conf->d_switches & DEF_SWT_verbose)
                        printf(" - standard header\n");
                }
            }
        }
    }

    return b_result;
}
// ***************************************************************************
// FUNCTION
//      mkh_action_conv_path
// PURPOSE
//
// PARAMETERS
//      PMKH_CONFIG   pst_conf  --
//      char        * psz_path  --
// RESULT
//      int --
// ***************************************************************************
int mkh_action_conv_path(PMKH_CONFIG pst_conf , char * psz_path)
{
    int     i_result        = 0;
    char    sz_path         [ PATH_MAX ];

    switch (*psz_path)
    {
        case '/':
        case '\\':
            if (!memcmp(DEF_CYGWIN_PREFIX, psz_path, DEF_CYGWIN_PREFIX_SZ))
            {
                strcpy(sz_path, psz_path + DEF_CYGWIN_PREFIX_SZ - 1);

                *(sz_path)      = *(sz_path + 1);
                *(sz_path + 1)  = ':';

                printf("%s\n", sz_path);
            }
            else
                printf("c:%s\n", psz_path);
            break;

        default:
            printf("%s\n", psz_path);
            break;
    }

    return i_result;
}
// ***************************************************************************
// FUNCTION
//      mkh_action_make_dep
// PURPOSE
//      Make dependencies
// PARAMETERS
//      PMKH_CONFIG  pst_conf       -- Pointer to Configuration structure
//      char*        psz_source_path -- Source path
//      char*        psz_target_path -- Target path
//      char*        psz_source_file -- Source file name
//      char*        psz_ext         -- Target extension
// RESULT
//      int  -- Zero if all is ok or error code
// ***************************************************************************
int mkh_action_make_dep(PMKH_CONFIG pst_conf,           char*   psz_source_path,
                        char*       psz_target_path,    char*   psz_source_file,
                        char*       psz_ext)
{
    int     i_result        = 1;
    FILE *  pst_src_file;
    FILE *  pst_dep_file;
    PFLIST  flist;
    char    sz_string       [ DEF_LINE_LEN + 1 ];
    char    sz_file         [ DEF_LINE_LEN + 1 ];
    char    sz_base_name  [ DEF_LINE_LEN + 1 ];

    if ((flist = flist_create()) != NULL)
    {
        sprintf(sz_string, "%s/%s", psz_source_path, psz_source_file);

        mkh_action_conv_path2(sz_string);

        if ((pst_src_file = fopen(sz_string, "rt")) != NULL)
        {
            sscanf(psz_source_file, "%256[^.]", sz_base_name);
            sprintf(sz_file, "%s/%s.dep", psz_target_path, psz_source_file);

            mkh_action_conv_path2(sz_file);

            //printf("DEP = %s\n", sz_file);   

            if ( ((pst_dep_file = fopen(sz_file, "w+t")) != NULL)   ||
                  (mkh_path_create_to_file(sz_file)                 &&
                  ((pst_dep_file = fopen(sz_file, "w+t")) != NULL)) )
            {
                //mkh_action_conv_path2(psz_target_path);

                fprintf(pst_dep_file, "%s/%s%s%s %s",
                        psz_target_path,
                        sz_base_name,
                        psz_ext,
                        ((pst_conf->d_switches & DEF_SWT_double_colons) ? "::" : ":"),
                        psz_source_file);

                // FIXM
                //pst_conf->d_switches |= DEF_SWT_verbose;

                i_result = !mkh_deps_extract(pst_conf, pst_dep_file, pst_src_file, psz_target_path, 0, flist);

                fclose(pst_dep_file);
            }
            else
                printf("%s: can't create dependency file %s\n", pst_conf->sz_name, sz_file);

            fclose(pst_src_file);
        }
        else
            printf("%s: can't open source file %s (%s)\n", pst_conf->sz_name, psz_source_file, sz_string);

        flist_destroy(flist);
    }
    else
        printf("cannot create flist\n");

    return i_result;
}
// ***************************************************************************
// FUNCTION
//      mkh_config_free
// PURPOSE
//
// PARAMETERS
//      PMKH_CONFIG pst_conf -- Pointer to configuration structure
// RESULT
//      PMKH_CONFIG -- Always NULL
// ***************************************************************************
PMKH_CONFIG mkh_config_free(PMKH_CONFIG pst_conf)
{
    if (pst_conf)
    {
        if (pst_conf->psz_hdrs_skip)
            free(pst_conf->psz_hdrs_skip);

        if (pst_conf->psz_hdrs_standard)
            free(pst_conf->psz_hdrs_standard);

        if (pst_conf->psz_hdrs_ext_target)
            free(pst_conf->psz_hdrs_ext_target);
    }

    return NULL;
}

// ***************************************************************************
// FUNCTION
//      mkh_config_load
// PURPOSE
//      Load configurartion
// PARAMETERS
//      PMKH_CONFIG pst_conf     --
//      long        d_conf_flags --
// RESULT
//      int
//          TRUE:  All is ok
//          FALSE: An error has occured
// ***************************************************************************
int  mkh_config_load(PMKH_CONFIG pst_conf, long  d_conf_flags)
{
    int     b_result        = TRUE;


    if (d_conf_flags & DEF_NEED_HDR_LISTS)
    {
        if ( !( ((pst_conf->psz_hdrs_skip       = mkh_list_load(pst_conf,
                                            "headers.skip")) != NULL)       &&
                ((pst_conf->psz_hdrs_standard   = mkh_list_load(pst_conf,
                                            "headers.standard")) != NULL)   &&
                ((pst_conf->psz_hdrs_ext_target = mkh_list_load(pst_conf,
                                            "headers.ext_target")) != NULL) ))
            b_result = FALSE;
    }

    return b_result;
}

// ***************************************************************************
// FUNCTION
//      main
// PURPOSE
//      Main function
// PARAMETERS
//      int     argc   -- Number of arguments
//      char  * argv[] -- Pointer to array of pointers to arguments
// RESULT
//      int -- Zero if all is ok or error code
// ***************************************************************************
int main(int i_argc, char *ppsz_argv[])
{
#define MAC_SWITCH(a, b)    case a: st_conf.d_switches |= b; break;

    int             i_retcode       = 1;
    int             i_arg_idx       = 1;
    long            d_conf_flags    = 0;
    char            b_error;
    MKH_CONFIG      st_conf;
    char*           psz_tmp;


    if (i_argc > 1)
    {
// -------------------------- Getting self name ------------------------------

        memset(&st_conf, 0, sizeof(MKH_CONFIG));

        *st_conf.sz_config_dir = 0;
        *st_conf.sz_name       = 0;

        if ( ((psz_tmp = strrchr(ppsz_argv[0], '/')) != NULL)   ||
             ((psz_tmp = strrchr(ppsz_argv[0], '\\')) != NULL)  )
            strcpy(st_conf.sz_name, psz_tmp + 1);
        else
            strcpy(st_conf.sz_name, ppsz_argv[0]);

// ---------------------- Getting config directory ---------------------------

        if ((psz_tmp = getenv(DEF_JKL_ROOT)) == NULL)
	{
	    psz_tmp = ".";
	}

            sprintf(st_conf.sz_config_dir, "%s/res", psz_tmp);

// -------------------------- Analysing command ------------------------------

            switch (*ppsz_argv[i_arg_idx++])
            {
                case 'd':
                case 'D':
                    st_conf.v_action = ACTION_MAKE_DEPS;
                    d_conf_flags     = DEF_NEED_HDR_LISTS;
                    break;

                case 'c':
                case 'C':
                    st_conf.v_action = ACTION_CONV_PATH;
                    break;
            }

            if (st_conf.v_action != ACTION_UNKNOWN)
            {
                switch (st_conf.v_action)
                {
                    case ACTION_CONV_PATH:
                        i_retcode = mkh_action_conv_path(&st_conf, ppsz_argv[i_arg_idx]);
                        break;

                    default:

        // -------------------------- Get configuration ------------------------------

                        if (mkh_config_load(&st_conf, d_conf_flags))
                        {
        // ------------------------- Processing switches -----------------------------

                            for (  b_error = FALSE;
                                   !b_error && (i_arg_idx < i_argc)  && (*ppsz_argv[i_arg_idx] == '-');
                                   i_arg_idx++)
                            {
                                switch (*(ppsz_argv[i_arg_idx] + sizeof(char)))
                                {
                                    MAC_SWITCH('d', DEF_SWT_double_colons);
                                    MAC_SWITCH('v', DEF_SWT_verbose);
                                }
                            }

                            if (!b_error)
                            {
        // -------------------------- Performing action ------------------------------

                                switch (st_conf.v_action)
                                {
                                    case ACTION_MAKE_DEPS:
                                        i_retcode = mkh_action_make_dep(&st_conf,
                                                ppsz_argv[i_arg_idx],     ppsz_argv[i_arg_idx + 1],
                                                ppsz_argv[i_arg_idx + 2], ppsz_argv[i_arg_idx + 3]);
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else
                                printf("%s: unrecognized switch: %s\n", st_conf.sz_name, ppsz_argv[i_arg_idx]);

                            mkh_config_free(&st_conf);
                        }
                        else
                            printf("%s: can not get configuration\n", st_conf.sz_name);

                        break;
                }
            }
            else
                printf("%s: first parameter should be a command\n", st_conf.sz_name);
    }
    else
        printf("Usage:\n"
                "    >deptool <cmd> [-sw -sw..] <src path> <tgt path> <src path/name> <ext>\n"
                "Where:\n"
                "    cmd           - command, see source code for details\n"
                "    -sw -sw..     - number of switches, see source code\n"
                "    src path      - first part of full path to the source file\n"
                "    tgt path      - first part of full path to the dependence and\n"
                "                    binary files\n"
                "    src path/name - relative part of path (to 'src path' and 'tgt path')\n"
                "                    and the name of source file\n"
                "    ext           - extension of the dependent binary file\n"
                "Note:\n"
                "    This utility using environment variable VPATH\n"
                "    to determine where it should search for header files\n"
                );

    return i_retcode;

#undef  MAC_SWITCH
}
